import { Point } from "./Geometry.js";
import { DrawingContext, Corner } from "./Drawing.js";
import { Player } from "./Player.js";
import { NormalEnemy } from "./NormalEnemy.js";

export enum Difficulty {
    Easy,
    Medium,
    Hard
}

export class EnemySpawner {
    private difficulty: Difficulty;
    private minDelay: number;
    private tickCount: number;
    private threshold: number;

    public get Difficulty() { return this.difficulty; }

        constructor(difficulty: Difficulty) {
        this.difficulty = difficulty;

        switch (difficulty) {
            case Difficulty.Easy:
                this.minDelay = 150;
                break;
            case Difficulty.Medium:
                this.minDelay = 120;
                break;
            case Difficulty.Hard:
                this.minDelay = 90;
                break;
            default:
                this.minDelay = 120;
                break;
        }

        this.tickCount = 0;
        this.threshold = this.minDelay + 66;
    }

    public nextTick(context: DrawingContext, player: Player): NormalEnemy | null {
        this.tickCount++;

        if (this.tickCount >= this.threshold) {
            this.tickCount -= this.threshold;
            if (this.threshold > this.minDelay)
                this.threshold -= 6;

            let rand: number = Math.random();

            let top: Point = new Point(context.Width / 2, -100);
            let left: Point = new Point(-100, context.Height / 2);
            let right: Point = new Point(context.Width + 100, context.Height / 2);
            let bottom: Point = new Point(context.Width / 2, context.Height + 100);

            let spawn: Point;

            switch (context.getCorner(player)) {
                case Corner.TopLeft:
                    spawn = (rand <= 0.5) ? right : bottom;
                    break;
                case Corner.TopRight:
                    spawn = (rand <= 0.5) ? left : bottom;
                    break;
                case Corner.BottomRight:
                    spawn = (rand <= 0.5) ? top : left;
                    break;
                case Corner.BottomLeft:
                    spawn = (rand <= 0.5) ? top : right;
                    break;
                default:
                    spawn = top;
                    break;
            }

            return new NormalEnemy(spawn, player, false);
        }

        return null;
    }
}