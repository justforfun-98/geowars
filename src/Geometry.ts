export interface ICloneable<T> {
    clone(): T;
}

export class Point implements ICloneable<Point> {
    public x: number;
    public y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    public clone(): Point {
        return new Point(this.x, this.y);
    }

    public distanceTo(target: Point): number {
        return pythagoras(target.x - this.x, target.y - this.y);
    }
}

export class Vector extends Point implements ICloneable<Vector> {
    protected xvec: number;
    protected yvec: number;
    private length: number;

    public get Xvec(): number { return this.xvec; }
    public get Yvec(): number { return this.yvec; }
    public get Length(): number { return this.length; }
    public get Endpoint(): Point { return new Point(this.x + this.xvec, this.y + this.yvec); }

    constructor(x: number, y: number, xvec: number, yvec: number) {
        super(x, y);
        this.xvec = xvec;
        this.yvec = yvec;

        this.length = pythagoras(this.x - (this.x + this.xvec), this.y - (this.y + this.yvec));
    }

    public clone(): Vector {
        return new Vector(this.x, this.y, this.xvec, this.yvec);
    }

    public setOrientation(target: Point): void {
        let dist: number = this.distanceTo(target);
        this.xvec = ((target.x - this.x) / dist) * this.length;
        this.yvec = ((target.y - this.y) / dist) * this.length;
    }

    public rotate(degrees: number): void {
        let radiant: number = (degrees * Math.PI) / 180;
        let sinRadiant: number = Math.sin(radiant);
        let cosRadiant: number = Math.cos(radiant);

        let xv: number = cosRadiant * this.xvec - sinRadiant * this.yvec; // cant change xvec, before next line is evaluatedd
        let yv: number = sinRadiant * this.xvec + cosRadiant * this.yvec;

        this.xvec = xv;
        this.yvec = yv;
    }

    public getRotationPoints(angles: number[]): Point[] {
        let res: Point[] = [];

        angles.forEach((angle: number): void => {
            this.rotate(angle);
            res.push(this.Endpoint);
        });

        return res;
    }
}

export function pythagoras(a: number, b: number): number {
    return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
}

export function contains(polygon: Point[], point: Point): boolean {
    if (polygon.length < 2)
        return false;

    let isRight: boolean = true;

    for (let i: number = 0; isRight && i < polygon.length; i++) {
        let next: number = (i + 1) % polygon.length;
        isRight = 0 <= (polygon[next].x - polygon[i].x) * (point.y - polygon[i].y) - (point.x - polygon[i].x) * (polygon[next].y - polygon[i].y);
    }

    return isRight;
}