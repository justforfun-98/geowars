import { MenuBar } from "./MenuBar.js";
import { DrawingContext } from "./Drawing.js";
import { Cursor } from "./Cursor.js";
import { Player, ShotControl } from "./Player.js";
import { Shot } from "./Shot.js";
import { NormalEnemy } from "./NormalEnemy.js";
import { EnemySpawner } from "./EnemySpawner.js";
import { Point } from "./Geometry.js";
import { Highscore, highscoreStorage } from "./HighscoreStorage.js";

let nextAnimationFrameRequestId: number;

let menuBar: MenuBar;
let context: DrawingContext;
let spawner: EnemySpawner;

const cursor: Cursor = new Cursor(-10, -10);
const player: Player = new Player();

const shots: Shot[] = [];
const enemies: NormalEnemy[] = [];

window.onload = function (): void {
    document.title = "GeoWars";
    document.body.style.margin = "0";
    document.body.style.padding = "0";
    document.body.style.border = "0";
    document.body.style.minWidth = "1160px";
    document.body.style.minHeight = "700px";
    document.body.style.fontFamily = "arial";
    document.body.innerHTML = MenuBar.HTML + "<canvas id='geocanvas'></canvas>";

    menuBar = new MenuBar();

    let element: HTMLElement | null = document.getElementById("geocanvas");
    if (element == null) throw new TypeError("The HTMLElement with the id geocanvas could not be obtained!");
    context = new DrawingContext(<HTMLCanvasElement>element);

    menuBar.addStartGameClickedListener(startGame);
    menuBar.addShotcontrolsChangedListener(shotcontrolsChangeHandler);
    context.addMouseMoveListener(cursor.updatePosition.bind(cursor));
    context.addMouseClickListener(mouseClickHandler);
    window.addEventListener("keydown", keyDownHandler);
    window.addEventListener("keypress", pauseGameHandler);

    window.addEventListener("resize", (): void => {
        if (menuBar.isGameOver) context.drawGameOverScreen();
        menuBar.isGameOver = true;
    });

    document.addEventListener("visibilitychange", (): void => {
        if (!menuBar.isGamePaused && document.visibilityState != "visible")
            pauseGameHandler(<KeyboardEvent> { keyCode: 112 }); // ugly
    });

    context.drawManual();
}

function shotcontrolsChangeHandler(): void {
    player.shotControl = menuBar.ShotControl;
}

function mouseClickHandler(): void {
    if (player.shotControl != ShotControl.Mouse || menuBar.isGameOver || menuBar.isGamePaused) return;
    shots.push(player.createShot());
    menuBar.playLazerSound();
}

function keyDownHandler(event: KeyboardEvent): void {
    if (player.shotControl != ShotControl.ArrowKeys || menuBar.isGameOver || menuBar.isGamePaused) return;
    switch (event.keyCode) {
        case 38: // ArrowUp
        case 37: // ArrowLeft
        case 40: // ArrowDown
        case 39: // ArrowRight
            player.keyDownHandler(event);
            shots.push(player.createShot());
            menuBar.playLazerSound();
            break;
    }
}

function pauseGameHandler(event: KeyboardEvent): void {
    if (!menuBar.isGameOver && event.keyCode == 112) { // p
        if (menuBar.isGamePaused) {
            menuBar.isGamePaused = false;
            menuBar.startMusic();
            window.requestAnimationFrame(tick);
        } else {
            menuBar.isGamePaused = true;
            menuBar.pauseMusic();
        }
    }
}

function startGame(): void {
    window.cancelAnimationFrame(nextAnimationFrameRequestId);

    menuBar.Score = 0;
    menuBar.isGamePaused = false;
    menuBar.isGameOver = false;
    menuBar.startMusic();

    spawner = new EnemySpawner(menuBar.Difficulty);

    player.x = context.Width / 2;
    player.y = context.Height / 2;

    cursor.x = player.x;
    cursor.y = player.y - 17;

    shots.length = 0;
    enemies.length = 0;

    nextAnimationFrameRequestId = window.requestAnimationFrame(tick);
}

function tick(): void {
    player.move(context);
    if (player.shotControl == ShotControl.Mouse) player.setOrientation(cursor);
    moveShots();
    moveEnemies();
    let e: null | NormalEnemy = spawner.nextTick(context, player);
    if (e != null) enemies.push(e);
    enemiesHitDetection();
    redraw();
    gameOverDetection();

    if (menuBar.isGamePaused)
        context.drawGamePausedScreen();
    else if (!menuBar.isGameOver)
        nextAnimationFrameRequestId = window.requestAnimationFrame(tick);
}

function moveShots(): void {
    for (let i: number = 0; i < shots.length; i++) {
        shots[i].move();
        if (!context.isInside(shots[i])) {
            shots.splice(i--, 1);
        }
    }
}

function moveEnemies(): void {
    for (let i: number = 0; i < enemies.length; i++) {
        enemies[i].Dir.rotate(2);
        enemies[i].move(context);
    }
}

function enemiesHitDetection(): void {
    for (let i: number = 0; i < enemies.length; i++) {
        for (let j: number = 0; j < shots.length; j++) {
            if (enemies[i].touches(shots[j])) {
                let newEnemies: NormalEnemy[] = enemies[i].hit();
                if (newEnemies.length > 0) {
                    newEnemies.forEach((enemy: NormalEnemy): number => enemies.push(enemy));
                    menuBar.Score += 50; // big enemy
                } else {
                    menuBar.Score += 75; // small enemy
                }
                enemies.splice(i--, 1);
                shots.splice(j--, 1);
                break;
            }
        }
    }
}

function gameOverDetection(): void {
    let playerShape: Point[] = player.getRotationPoints([140, 80, 140]);

    for (let i: number = 0; !menuBar.isGameOver && i < enemies.length; i++)
        playerShape.forEach((point: Point): void => {
            menuBar.isGameOver = menuBar.isGameOver || enemies[i].contains(point);
        });

    if (menuBar.isGameOver) endGame();
}

function redraw(): void {
    context.clear();
    //cursor.draw(context);
    player.draw(context);
    shots.forEach((shot: Shot): void => shot.draw(context));
    enemies.forEach((enemy: NormalEnemy): void => enemy.draw(context));
}

function endGame(): void {
    menuBar.isGameOver = true;
    menuBar.stopMusic();

    context.drawGameOverScreen();

    shots.length = 0;
    enemies.length = 0;

    highscoreStorage.addHighscore(new Highscore(menuBar.PlayerName, spawner.Difficulty, menuBar.Score));
}