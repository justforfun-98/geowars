import { Point, Vector } from "./Geometry.js";

export enum Corner {
    TopLeft,
    TopRight,
    BottomRight,
    BottomLeft
}

export class DrawingContext {
    private context: CanvasRenderingContext2D;

    private get Canvas() { return this.context.canvas; }

    public get Width(): number { return this.Canvas.width; }
    public get Height(): number { return this.Canvas.height; }

    public set Color(color: string | CanvasGradient | CanvasPattern) { this.context.strokeStyle = color; }
    public set Thickness(thickness: number) { this.context.lineWidth = thickness; }
    public set Opacity(opacity: number) { this.context.globalAlpha = opacity; }

    public constructor(canvas: HTMLCanvasElement) {
        let c: CanvasRenderingContext2D | null = canvas.getContext("2d");

        if (c == null)
            throw new TypeError("The CanvasRenderingContext2D could not be obtained for the canvas with the id " + canvas.id + "!");

        this.context = c;

        canvas.style.cursor = "crosshair";
        canvas.style.display = "block";

        this.windowResizeHandler();
        window.addEventListener("resize", this.windowResizeHandler.bind(this));
    }

    private windowResizeHandler(): void {
        this.Canvas.width = Math.max(document.documentElement.clientWidth, 1160);
        this.Canvas.height = Math.max(Math.ceil(document.documentElement.clientHeight * 0.92), 782);
    }

    public isHorizontallyInside(point: Point, xoffset: number = 0): boolean {
        return (point.x + xoffset >= 0) && (point.x + xoffset < this.Width);
    }

    public isVerticallyInside(point: Point, yoffset: number = 0): boolean {
        return (point.y + yoffset >= 0) && (point.y + yoffset < this.Height);
    }

    public isInside(point: Point, xoffset: number = 0, yoffset: number = 0): boolean {
        return this.isHorizontallyInside(point, xoffset) && this.isVerticallyInside(point, yoffset);
    }

    public isSquareHorizontallyInside(vector: Vector, xoffset: number = 0): boolean {
        return (vector.x + xoffset - vector.Length >= 0) && (vector.x + xoffset + vector.Length < this.Width);
    }

    public isSquareVerticallyInside(vector: Vector, yoffset: number = 0): boolean {
        return (vector.y + yoffset - vector.Length >= 0) && (vector.y + yoffset + vector.Length < this.Height);
    }

    public isSquareInside(vector: Vector, xoffset: number = 0, yoffset: number = 0): boolean {
        return this.isSquareHorizontallyInside(vector, xoffset) && this.isSquareVerticallyInside(vector, yoffset);
    }

    public getCorner(point: Point): Corner {
        if (point.x <= this.Width / 2)
            if (point.y <= this.Height / 2)
                return Corner.TopLeft;
            else
                return Corner.BottomLeft;
        else
            if (point.y <= this.Height / 2)
                return Corner.TopRight;
            else
                return Corner.BottomRight;
    }

    public drawLine(start: Point, end: Point): void {
        this.context.beginPath();
        this.context.moveTo(start.x, start.y);
        this.context.lineTo(end.x, end.y);
        this.context.closePath();
        this.context.stroke();
    }

    public drawPolygon(points: Point[]): void {
        if (points.length < 2)
            return;

        this.context.beginPath();
        this.context.moveTo(points[0].x, points[0].y);

        for (let i: number = 1; i < points.length; i++)
            this.context.lineTo(points[i].x, points[i].y);

        this.context.closePath();
        this.context.stroke();
    }

    public clear(): void {
        this.context.clearRect(0, 0, this.Width, this.Height);
    }

    public drawManual(): void {
        let lineHeight: number = 36;
        let lines: string[] = [
            "Use the W, A, S, D keys to move",
            "Use the P key to pause the game",
            "Use the mouse to aim and click to shoot",
            "Alternatively you can use the arrow keys to aim and shoot",
            "A big enemy moves slow and gives you 50 points",
            "When you kill a big enemy, he will split into four smaller ones",
            "A small enemy moves faster than a big one and gives you 75 points",
            "When an enemy touches you, the game is over",
            "The three difficulty modes differ in terms of the enemy spawn rate",
            "Each of them has its own high score table"
        ];
        let freeLine: number = (this.Height - lineHeight * lines.length) / (lines.length + 1);

        this.context.font = lineHeight + "px Arial";
        let x: number = this.Width / 2 - Math.max(...lines.map((line: string): number => this.context.measureText(line).width)) / 2;

        for (let i: number = 0; i < lines.length; i++)
            this.context.fillText(lines[i], x, (i + 1) * (freeLine + lineHeight));
    }

    private drawFreezeScreen(message: string): void {
        this.Opacity = 0.5;
        this.context.fillStyle = "white";

        this.context.fillRect(0, 0, this.Width, this.Height);

        this.Opacity = 1;
        this.context.fillStyle = "black";

        this.context.font = "60px Arial";
        this.context.fillText(message, this.Width / 2 - this.context.measureText(message).width / 2, this.Height / 2);
    }

    public drawGamePausedScreen(): void {
        this.drawFreezeScreen("Game Paused");
    }

    public drawGameOverScreen(): void {
        this.drawFreezeScreen("Game Over");
    }

    public addMouseMoveListener(listener: (event: MouseEvent) => any): void {
        this.Canvas.addEventListener("mousemove", listener);
    }

    public removeMouseMoveListener(listener: (event: MouseEvent) => any): void {
        this.Canvas.removeEventListener("mousemove", listener);
    }

    public addMouseClickListener(listener: (event: MouseEvent) => any): void {
        this.Canvas.addEventListener("click", listener);
    }

    public removeMouseClickListener(listener: (event: MouseEvent) => any): void {
        this.Canvas.removeEventListener("click", listener);
    }
}

export interface IDrawable {
    draw(context: DrawingContext): void;
}