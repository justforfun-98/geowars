import { Vector } from "./Geometry.js";
import { DrawingContext, IDrawable } from "./Drawing.js";

export class Shot extends Vector implements IDrawable {
    public constructor(vector: Vector) {
        super(vector.x, vector.y, vector.Xvec, vector.Yvec);
    }

    public move(): void {
        this.x += this.xvec;
        this.y += this.yvec;
    }

    public draw(context: DrawingContext): void {
        context.Color = "red";
        context.drawLine(this, this.Endpoint);
        context.Color = "black";
    }
}